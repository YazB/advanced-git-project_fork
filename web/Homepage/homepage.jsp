<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Homepage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
              integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
                integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>

        <style>


        </style>
    </head>
    <body>
        <header class="bg-secondary text-center rounded-0">
            <h1 class="display-4"> Gratitude Diary</h1><br>
            <p class="lead">An application by you</p>

        </header>


        <div class="container text-center mb-0">
            <form method="post" action="/login">
                <input type="password" name="login" class="form-control" id="logincode"
                       placeholder="Login Code"><br><br><br><br>

                <input type="submit" value="Login" name="butlogin" class="btn btn-primary "> <br><br><br><br>

                <p id="or">----- or ----- </p><br><br><br>
                <button type="submit" name="butCreateAccount" class="btn btn-primary btn-lg">Large button</button>
            </form>
        </div>


        <%--<%@include file="yaz.jsp"%>--%>


    </body>
</html>