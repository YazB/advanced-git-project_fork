<%--
  Created by IntelliJ IDEA.
  project1.User: sl297
  Date: 17/01/2019
  Time: 1:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>

    </head>
    <body>

        <ul>
            <c:if test="${myMemories == null}">
                <li>No memories yet! Create some!</li>
            </c:if>
            <c:if test="${myMemories != null}">
                <c:forEach items="${myMemories}" var="memory">
                    <li>${memory.content}</li>
                </c:forEach>
            </c:if>
        </ul>
    </body>
</html>