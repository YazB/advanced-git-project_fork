package project1;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "project1.LoginServlet")
public class LoginServlet extends HttpServlet {
    // this servlet only wants post requests?? check this with group members

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in the loginservlet dopost");

        System.out.println("in the loginservlet get");

        int code;
        UserDAO dao = new UserDAO(getServletContext());


        // if the login button was pressed
        if (request.getParameterMap().containsKey("butlogin")) {

            System.out.println("loggin in");
            //todo check what the parameter is actually called in the form
            code = Integer.parseInt(request.getParameter("login"));
System.out.println("code = " +code);

            // check whether the provided code exists in the database, if true do something
            if (dao.checkLoginCode(code)) {
                // login that use3r
                // send them to yaz.jsp or entries servlet???
                Cookie cookie = new Cookie("userid", ""+code);

                response.addCookie(cookie);
                response.sendRedirect("yaz.jsp");


            } else {
                //if the code was wrong, redirect to login page with message saying ty try again
                //https://stackoverflow.com/questions/14632252/servlet-redirect-to-same-page-with-error-message
                request.setAttribute("errorMessage", "Couldn't find that user code. Please try again.");
                // get back to the main page using forward
                request.getRequestDispatcher("/Homepage/homepage.jsp").forward(request, response);
            }


        } else {
            //if that parameter wasn't provided (clicked create account) then create a new user
            System.out.println("creating new user");
            dao.newUser();

        }
    }





    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
